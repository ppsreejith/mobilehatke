//buyLater is wishlist content, buyLater starts here
(function(){
if(typeof(Storage)!=="undefined")
{
if(typeof(localStorage.hatke_favourites) == "undefined")
window.buyLater = [];
else
window.buyLater = localStorage.hatke_favourites.split("+#+#+");
}
else window.buyLater = [];
}());
//buyLater ends here
//appSettings to be loaded from localstorage
var appSettings = (function(){
	var _appSettings = {filter:'false'};
	if(typeof(Storage)!=="undefined")
	{
	if(typeof(localStorage.hatke_settings) !== "undefined")
	_appSettings = JSON.parse(localStorage.hatke_settings);
	}
	return function(obj){
		obj = obj || {};
		if(Object.keys(obj).length !==0 )localStorage.hatke_settings = JSON.stringify($.extend(_appSettings,obj));
		return _appSettings;
	}
}());
function updateSettings(){
	var a = appSettings({filter:$('#set_filter').val()});
	if(a.filter == 'true') $('.ui-listview-filter').show();
	else $('.ui-listview-filter').hide();
}
function setSettings(){
	var a = appSettings({});
	$('#set_filter').val(a.filter);
}
//appSettings end
//Settings contains search information to be sent to server,Settings starts here
var settings = (function(){
	var _settings = {search:"apple",start:0,limit:30};
//conditional function extend here
	return function(_obj){
		_obj = _obj || {};
		$.extend(_settings,_obj);
		return _settings;
	};
}());
//Settings ends here
//overview settings here
function fillOverview(a){
$("#over_image").attr({"src":a.image});
$("#over_name").html(a.prod);
$("#over_price").html(a.price);
$("#over_from").html('Seller');
$("#over_link").attr({"href":a.link});
}
//overview ends here
//hash changing
function changeHash(){
	if(location.search.indexOf('?') !== -1){
		$.mobile.loading('show');
		var query = location.search.substring(1);
		var set = {search:"",start:0};
		$.extend(set,settings(JSON.parse('{"' + decodeURI(query).replace(/&/g, "\",\"").replace(/=/g,"\":\"") + '"}')));
		var message = $.param(set);
		$.ajax({
			url: "lol.php",
			type:'get',
			dataType:'json',
			data:message,
			cache:false,
			success: function(result){
				myData = eval(result);
				setProducts(myData);
				$.mobile.loading('hide');
				$(document).off('mouseover');
			}
		});
	}
}
//hash changing ends here
//Function to fill wishlist, Wishlist starts here
(function(){
var counter = 0;
window.wishlistFill = function(){
var data="";
for(var i in buyLater){
arr = $.parseJSON(buyLater[i]);
data+="<li name = '"+JSON.stringify(arr)+"'><a href='"+arr.link+"' rel='external' target='_blank'><i>Seller</i><img src='"+arr.image+"'/><h1>"+arr.prod+"</h1><p>"+arr.price+"</p></a></li>";
}
if(counter == 0){
$("#favitems").html(data).trigger('create');
counter = 1;
}
else{
$("#favitems").html(data).listview('refresh');
}
};
}());
//Wishlist ends here
//Function to fill Products, setProducts starts here
function setProducts(arr){
var data="";
for(var i in arr){
data+="<li name='"+JSON.stringify(arr[i])+"' data-corners='false' data-shadow='false' data-iconshadow='true' data-wrapperels='div' data-icon='false' data-iconpos='right' data-theme='c' class='ui-btn ui-btn-up-c ui-btn-icon-right ui-li ui-li-has-alt ui-li-has-thumb'><div class='ui-btn-inner ui-li ui-li-has-alt'><div class='ui-btn-text'><a href='"+arr[i].link+"' rel='external' target='_blank' class='ui-link-inherit'><i class='ui-li-aside' >Seller</i><img src='"+arr[i].image+"' class='ui-li-thumb' /><h1 class='ui-li-heading'>"+arr[i].prod+"</h1><p class='ui-li-desc'>"+arr[i].price+"</p></a></div></div><a href='#' name='"+JSON.stringify(arr[i])+"'  class='buy_later ui-li-link-alt ui-btn ui-btn-up-c ui-btn-icon-notext' data-corners='false' data-shadow='false' data-iconshadow='true' data-wrapperels='span' data-icon='false' data-iconpos='notext' data-theme='c'><span class='ui-btn-inner'><span class='ui-btn-text'></span><span data-corners='true' data-shadow='true' data-iconshadow='true' data-wrapperels='span' data-icon='arrow-r' data-iconpos='notext' data-theme='b' title='' class='ui-btn ui-btn-up-b ui-shadow ui-btn-corner-all ui-btn-icon-notext'><span class='ui-btn-inner ui-btn-corner-all'><span class='ui-btn-text'></span><span class='ui-icon ui-icon-arrow-r ui-icon-shadow'>&nbsp;</span></span></span></span></a></li>";
}
$("#items",$.mobile.activePage).empty().append(data);
}
readyFlag = 0; //flag to create document ready
//setProducts ends here
//init prog deals with document.ready event,init prog starts here
function initProg(){
//buy later option starts here
$('body').on('tap','.buy_later',function(e){
e.preventDefault();
var ret = $(this).attr("name");
if($.inArray(ret,buyLater) == -1){
buyLater.push(ret);
localStorage.hatke_favourites = buyLater.join("+#+#+");
}
$(this).children().first().children().first().next().addClass("ui-btn-up-c");
return false;
});
//buy later ends here. 
//filter settings start here
$('#set_filter').on('change',function(){
updateSettings();
});
//filter ends here
//search menu button starts here
$('body').on('tap','#searchButton',(function(){
	var a = $.mobile.activepage;
	return function(e){
		e.preventDefault();
		var l = $('div#searchmenu',a);
		if(l.css("max-height") == "0px"){
			l.css({"max-height":"200px"});
		}
		else{
			l.css({"max-height":"0px"});
		}
		return false;
	}
}()));
//search menu button ends here
//wishlist trigger starts here
$("body").on('tap','#wishlistButton',function(e){
wishlistFill();
});
//wishlist trigger ends here
//search entry starts here
$("body").on('tap','li',function(e){
var a = JSON.parse($(this).attr("name"));
fillOverview(a);
$.mobile.changePage($("#overview"),{role:"dialog"});
return false;
});
$("body").on('tap','#submitSearch',(function(){
	var ourSet = {};
	return function(e){
		e.preventDefault();
		var s = $("input#searchBar",$.mobile.activePage);
		var p = $("input#rangeBar",$.mobile.activePage);
		if(s.val().length > 0) ourSet.search = s.val();
		ourSet.start = p.val();
		var message = $.param(settings(ourSet));
		//$.mobile.changePage('index.html',{transition:'fade',data:message});
		location.search=message;
		return false;
	};
}())
);
//search entry ends here
}
//init prog ends here
//Main Body, Page Init events
$(document).on('pageinit',function(){
setSettings();
updateSettings();
//setTimeout(function(){$("#page").css({"padding-top":$("header").outerHeight()+"px"});},500);
//js starts here
if(readyFlag == 0){
readyFlag = 1;
initProg();
}
changeHash();
//js ends here
});